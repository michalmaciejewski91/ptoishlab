package mm.lab.ptoish.exception;

import java.util.Arrays;

public class MissingArgException extends Exception {

    public MissingArgException(String... s) {
        super("Provide required arguments: " + Arrays.asList(s).toString());
    }
}
