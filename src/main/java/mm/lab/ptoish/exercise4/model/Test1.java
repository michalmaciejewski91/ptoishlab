package mm.lab.ptoish.exercise4.model;

import mm.lab.ptoish.exercise3.model.Testable;
import org.apache.log4j.Logger;

public class Test1 implements Testable {

    public static final Logger LOG = Logger.getLogger(Test1.class);

    @Override
    public void test() {
        LOG.info("Test 1 msg");
    }

}
