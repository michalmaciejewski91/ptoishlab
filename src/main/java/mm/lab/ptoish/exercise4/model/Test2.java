package mm.lab.ptoish.exercise4.model;

import mm.lab.ptoish.exercise3.model.Testable;
import org.apache.log4j.Logger;

public class Test2 implements Testable {

    public static final Logger LOG = Logger.getLogger(Test2.class);

    @Override
    public void test() {
        LOG.info("Test 2 msg");
    }

}
