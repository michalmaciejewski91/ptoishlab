package mm.lab.ptoish.exercise4.model;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import org.apache.log4j.Logger;

public class DynamicProxy implements InvocationHandler {

    private static final Logger LOG = Logger.getLogger(DynamicProxy.class);

    private Object target;

    public DynamicProxy(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        LOG.info("Called method: " + method.getName());
        return method.invoke(target, args);
    }
}
