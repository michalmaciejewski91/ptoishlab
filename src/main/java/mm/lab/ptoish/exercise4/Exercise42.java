package mm.lab.ptoish.exercise4;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.List;
import org.apache.log4j.Logger;

public class Exercise42 {

    public static final Logger LOG = Logger.getLogger(Exercise42.class);

    public static void main(String[] args) {
        LOG.info("____________________________________________");
        LOG.info("Exercise 4.2");

        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        ReferenceQueue referenceQueue = new ReferenceQueue();

        SoftReference softRef = new SoftReference(list);
        WeakReference weakReference = new WeakReference(list);
        PhantomReference phantomReference = new PhantomReference(list, referenceQueue);
    }

}
