package mm.lab.ptoish.exercise4;

import java.lang.reflect.Proxy;
import mm.lab.ptoish.exercise3.model.Testable;
import mm.lab.ptoish.exercise4.model.DynamicProxy;
import mm.lab.ptoish.exercise4.model.Test1;
import mm.lab.ptoish.exercise4.model.Test2;
import org.apache.log4j.Logger;

public class Exercise41 {

    public static final Logger LOG = Logger.getLogger(Exercise41.class);

    public static void main(String[] args) {
        LOG.info("____________________________________________");
        LOG.info("Exercise 4.1");

        Test1 test1 = new Test1();
        Test2 test2 = new Test2();

        Testable test1Proxy = (Testable) Proxy.newProxyInstance(
            test1.getClass().getClassLoader(),
            new Class[]{Testable.class},
            new DynamicProxy(test1));

        Testable test2Proxy = (Testable) Proxy.newProxyInstance(
            test1.getClass().getClassLoader(),
            new Class[]{Testable.class},
            new DynamicProxy(test2));

        test1Proxy.test();
        test2Proxy.test();
    }
}
