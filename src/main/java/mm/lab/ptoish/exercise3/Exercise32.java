package mm.lab.ptoish.exercise3;

import mm.lab.ptoish.exercise3.model.Test;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Exercise32 {

    private static final Logger LOG = Logger.getLogger(Exercise32.class);

    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException, NoSuchFieldException {
        LOG.info("____________________________________________");
        LOG.info("Exercise 3.2");

        Object testObject = new Test();

        Method method = testObject.getClass().getMethod("test");
        method.invoke(testObject);

        Field publicStringField = testObject.getClass().getDeclaredField("publicStringField");
        LOG.info("public field value = " + publicStringField.get(testObject));

        Field privateStringField = testObject.getClass().getDeclaredField("privateStringField");
        privateStringField.setAccessible(true);
        LOG.info("private field value = " + privateStringField.get(testObject));
    }
}
