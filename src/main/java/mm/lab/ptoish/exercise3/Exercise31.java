package mm.lab.ptoish.exercise3;

import mm.lab.ptoish.exercise3.model.Cache;
import org.apache.log4j.Logger;

public class Exercise31 {

    private static final Logger LOG = Logger.getLogger(Exercise31.class);

    public static void main(String[] args) {
        LOG.info("____________________________________________");
        LOG.info("Exercise 3.1");

        Cache testCache = new Cache();
        for (int i = 1; i <= 10; i++) {
            testCache.put("key " + i, "value " + i);
            LOG.info(testCache);
        }

        for (int i = 1; i <= 10; i++) {
            testCache.put("key " + i, 10);
            LOG.info(testCache);
        }
    }
}
