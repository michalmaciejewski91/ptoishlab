package mm.lab.ptoish.exercise3.model;

public interface Testable {

    public void test();
}
