package mm.lab.ptoish.exercise3.model;

import org.apache.log4j.Logger;

public class Test implements Testable {

    private static final Logger LOG = Logger.getLogger(Test.class);

    public String publicStringField = "public string";
    private String privateStringField = "private string";

    @Override
    public void test() {
        LOG.info("!!! TEST !!!");
    }
}
