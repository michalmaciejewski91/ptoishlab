package mm.lab.ptoish;

import com.google.common.base.Stopwatch;

public class Benchmark {

    private Benchmark() {

    }

    public static String call(Runnable runnable) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        runnable.run();
        stopwatch.stop();
        return "Execution time: " + stopwatch.toString();
    }
}
