package mm.lab.ptoish;

import mm.lab.ptoish.exception.MissingArgException;
import mm.lab.ptoish.exercise1.Exercise11;
import mm.lab.ptoish.exercise1.Exercise12;
import mm.lab.ptoish.exercise2.Exercise21;
import mm.lab.ptoish.exercise2.Exercise22;
import mm.lab.ptoish.exercise3.Exercise31;
import mm.lab.ptoish.exercise3.Exercise32;
import mm.lab.ptoish.exercise4.Exercise41;
import mm.lab.ptoish.exercise4.Exercise42;
import org.apache.log4j.Logger;

public class PTOiSHRunner {

    private static final Logger LOG = Logger.getLogger(PTOiSHRunner.class);

    public static void main(String[] args) throws Throwable {
        if (args.length >= 1) {
            switch (args[0]) {
                case "1.1":
                    Exercise11.main(args);
                    break;
                case "1.2":
                    Exercise12.main(args);
                    break;
                case "2.1":
                    Exercise21.main(args);
                    break;
                case "2.2":
                    Exercise22.main(args);
                    break;
                case "3.1":
                    Exercise31.main(args);
                    break;
                case "3.2":
                    Exercise32.main(args);
                    break;
                case "4.1":
                    Exercise41.main(args);
                    break;
                case "4.2":
                    Exercise42.main(args);
                    break;
                default:
                    LOG.info("Wrong exerciseNumber");
            }
        } else {
            throw new MissingArgException("exerciseNumber");
        }
    }
}
