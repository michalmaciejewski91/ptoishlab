package mm.lab.ptoish.exercise2;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;
import mm.lab.ptoish.Benchmark;
import mm.lab.ptoish.exception.MissingArgException;
import org.apache.log4j.Logger;

public class Exercise21 {

    private static final Logger LOG = Logger.getLogger(Exercise21.class);

    private HashSet<Integer> hashSet = new HashSet<>();
    private LinkedHashSet<Integer> linkedHashSet = new LinkedHashSet<>();
    private TreeSet<Integer> treeSet = new TreeSet<>();

    private int setSize;
    private int middleElementIndex;

    private Exercise21(String[] args) throws MissingArgException {
        if (args.length == 2) {
            setSize = Integer.parseInt(args[1]);
            middleElementIndex = setSize / 2;
        } else {
            throw new MissingArgException("setSize");
        }
    }

    public static void main(String[] args) throws MissingArgException {
        LOG.info("____________________________________________");
        LOG.info("Exercise 2.1");

        Exercise21 test = new Exercise21(args);
        test.init();
        test.perform();
    }

    private void init() {
        LOG.info("Set size: " + setSize);
        for (int i = 1; i <= setSize; i++) {
            hashSet.add(i);
            linkedHashSet.add(i);
            treeSet.add(i);
        }
    }

    private void perform() {
        performAddTest();
        performRemoveTest();
        performBrowsingTest();
        performContainingTest();
    }

    private void performAddTest() {
        LOG.info("");
        LOG.info("ADD test");
        LOG.info("HashSet - " + Benchmark.call(() -> add(hashSet)));
        LOG.info("LinkedHashSet - " + Benchmark.call(() -> add(linkedHashSet)));
        LOG.info("TreeSet - " + Benchmark.call(() -> add(treeSet)));
    }

    private void performRemoveTest() {
        LOG.info("");
        LOG.info("REMOVE test");
        LOG.info("HashSet - " + Benchmark.call(() -> remove(hashSet)));
        LOG.info("LinkedHashSet - " + Benchmark.call(() -> remove(linkedHashSet)));
        LOG.info("TreeSet - " + Benchmark.call(() -> remove(treeSet)));
    }

    private void performBrowsingTest() {
        LOG.info("");
        LOG.info("BROWSING test");
        LOG.info("HashSet - " + Benchmark.call(() -> browse(hashSet)));
        LOG.info("LinkedHashSet - " + Benchmark.call(() -> browse(linkedHashSet)));
        LOG.info("TreeSet - " + Benchmark.call(() -> browse(treeSet)));
    }

    private void performContainingTest() {
        LOG.info("");
        LOG.info("CONTAINING test");
        LOG.info("HashSet - " + Benchmark.call(() -> contains(hashSet)));
        LOG.info("LinkedHashSet - " + Benchmark.call(() -> contains(linkedHashSet)));
        LOG.info("TreeSet - " + Benchmark.call(() -> contains(treeSet)));
    }

    private void add(Set set) {
        set.add(999999999);
    }

    private void remove(Set set) {
        Iterator iterator = set.iterator();
        Object middleElement = null;
        for (int i = 0; i < middleElementIndex; i++) {
            middleElement = iterator.next();
        }
        set.remove(middleElement);
    }

    private void browse(Set set) {
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            iterator.next();
        }
    }

    private boolean contains(Set set) {
        return set.contains(999999998);
    }
}
