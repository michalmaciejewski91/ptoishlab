package mm.lab.ptoish.exercise1.model;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Person implements Externalizable {

    public static final long serialVersionUID = 1234L;

    private final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy");

    private String firstName;
    private String lastName;
    private Date birthday;
    private int age;

    public Person() {

    }

    public Person(String firstName, String lastName, Date birthday) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        calculateAndSetAge(birthday);
    }

    private void calculateAndSetAge(Date birthday) {
        Calendar currentCalendar = Calendar.getInstance();
        Calendar birthdayCalendar = Calendar.getInstance();
        birthdayCalendar.setTime(birthday);
        this.age = (currentCalendar.get(Calendar.YEAR) - birthdayCalendar.get(Calendar.YEAR));
    }

    @Override
    public String toString() {
        return firstName + " " + lastName + ", " + SDF.format(birthday) + ", Age: " + age;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(this.firstName);
        out.writeObject(this.lastName);
        out.writeObject(this.birthday);
        out.writeInt(this.age);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        this.firstName = (String) in.readObject();
        this.lastName = (String) in.readObject();
        this.birthday = (Date) in.readObject();
        this.age = in.readInt();
    }
}
