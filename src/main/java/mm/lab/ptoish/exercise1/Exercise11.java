package mm.lab.ptoish.exercise1;

import mm.lab.ptoish.exercise1.model.Person;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.Calendar;

public class Exercise11 {

    private static final Logger LOG = Logger.getLogger(Exercise11.class);

    private static final String SERIALIZED_OBJECTS_FOLDER_PATH = "output/serializedObjects/";

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        LOG.info("____________________________________________");
        LOG.info("Exercise 1.1");

        Person michal = createPerson("Michał", "Maciejewski", 21, 9, 1991);
        serializeToFile(michal, "michalExternalization.ser");

        Person deserializedMichal = deserializeFromFile("michalExternalization.ser");
        LOG.info(deserializedMichal);
    }

    private static void serializeToFile(Person person, String fileName) throws IOException {
        FileOutputStream fos = new FileOutputStream(SERIALIZED_OBJECTS_FOLDER_PATH + fileName, false);
        try (ObjectOutputStream outputStream = new ObjectOutputStream(fos)) {
            outputStream.writeObject(person);
        }
    }

    private static Person deserializeFromFile(String fileName) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(SERIALIZED_OBJECTS_FOLDER_PATH + fileName);
        Person person;
        try (ObjectInputStream inputStream = new ObjectInputStream(fis)) {
            person = (Person) inputStream.readObject();
        }
        return person;
    }

    private static Person createPerson(String firstName, String lastName, int day, int month, int year) {
        Calendar birthDay = Calendar.getInstance();
        birthDay.set(year, month - 1, day);

        return new Person(firstName, lastName, birthDay.getTime());
    }

}
