package mm.lab.ptoish.exercise1;

import mm.lab.ptoish.Benchmark;
import org.apache.log4j.Logger;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Exercise12 {

    private static final Logger LOG = Logger.getLogger(Exercise12.class);

    private static final String BIG_FILE_FOLDER_PATH = "output/";
    private static final int FILE_SIZE = 500000000;
    private static final Charset CHARSET = Charset.forName("UTF-8");

    public static void main(String[] args) {
        LOG.info("____________________________________________");
        LOG.info("Exercise 1.2");

        String someBigFilePath = BIG_FILE_FOLDER_PATH + "someBigFile.txt";
        createTestFile(someBigFilePath, FILE_SIZE);

        LOG.info("BufferedRead - " + Benchmark.call(() -> readFileUsingBufferedRead(someBigFilePath)));
        LOG.info("NIOLib - " + Benchmark.call(() -> readFileUsingNIOLib(someBigFilePath)));
        LOG.info("MemoryMappedFile - " + Benchmark.call(() -> readFileUsingMemoryMapFile(someBigFilePath)));
    }

    private static File createTestFile(String fileName, long size) {
        deleteIfExist(fileName);
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(fileName, "rw")) {
            randomAccessFile.setLength(size);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
        File testFile = new File(fileName);
        LOG.info("Test file size: " + testFile.length());
        return testFile;
    }

    private static void deleteIfExist(String fileName) {
        try {
            Path path = Paths.get(fileName);
            if (path.toFile().exists()) {
                Files.delete(path);
            }
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    private static void readFileUsingBufferedRead(String filePath) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {
            char[] charBuf = new char[FILE_SIZE];
            bufferedReader.read(charBuf, 0, FILE_SIZE);

            String fileContent = new String(charBuf);
            System.out.println(fileContent);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    private static void readFileUsingNIOLib(String filePath) {
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(filePath, "rw")) {
            FileChannel fileChannel = randomAccessFile.getChannel();
            int fileChannelSize = Math.toIntExact(fileChannel.size());

            ByteBuffer byteBuffer = ByteBuffer.allocate(fileChannelSize);
            while (fileChannel.read(byteBuffer) > 0) {
                byteBuffer.rewind();

                String fileContent = CHARSET.decode(byteBuffer).toString();
                System.out.println(fileContent);

                byteBuffer.flip();
            }
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    private static void readFileUsingMemoryMapFile(String filePath) {
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(filePath, "rw")) {
            FileChannel fileChannel = randomAccessFile.getChannel();
            int fileChannelSize = Math.toIntExact(fileChannel.size());

            MappedByteBuffer mappedByteBuffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileChannelSize);
            byte[] bytesBuf = new byte[fileChannelSize];
            mappedByteBuffer.get(bytesBuf, 0, fileChannelSize);

            String content = new String(bytesBuf);
            System.out.println(content);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }
}
