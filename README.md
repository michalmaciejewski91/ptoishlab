# PTOiSHLab
 Project created for university purpose

## Exercise 1.1
Serialization vs externalization
#### SKILLS:
After this exercise you get the ability to persist objects by means of serialization and
externalization.
#### GOALS AND OBJECTIVES:
The goal is to become familiar with the practical implementation of persisting data
using serialization and externalization, and their comparison.
#### WORK STEPS:
* Create a class describing a person (first and last name, birthday, age).
* Serialize object of this type to a file.
* Check if you can deserialize the object.
* Save the same object using externalization to file, providing your own read
and write methods.
* Which code fragment do you need to change?
* Deexternalize the object.
* Compare size of both files


## Exercise 1.2
Efficient file access
#### SKILLS:
After completing this exercise, you get the ability to effectively read data from
streams.
#### GOALS AND OBJECTIVES:
The goal is the practical implementation and comparison in terms of performance of
different ways to read data from streams.
#### WORK STEPS:
* Write a program, that creates a large file with binary data (several hundred MB
big). Every run should generate new data.
* Using the generated file test the performance of different file read methods:
** buffered read
** using NIO library (buffers and channels)
** memory mapped files
* Write an appropriate benchmark:
** try to make it as generic as possible to reuse it in other exercises


## Exercise 2.1
Collections
### SKILLS:
After completing this exercise, you get knowledge about the efficiency of using
different implementations of collections performing various operations.
### GOALS AND OBJECTIVES:
The goal is to provide areas of application of different collections of type Set
performing basic operations.
### WORK STEPS:
* Conduct performance tests on known implementations of sets (Set)
concerning operations:
** adding elements
** removing elements
** browsing
** checking if element exists
* Perform tests using benchmarks. Keep in mind common problems with microbenchmarking
and warm and the Java Virtual Machine. You can use classes
created for previous exercises.
* Write results to file for documentation.
* Perform tests on sets of size:
** 10
** 100
** 1 000
** 10 000
** … - as long as the response times are reasonable
* We are interested in a the time of a single operation performed on the Set of a
given size. In the lectures you find average times for different collection sizes.
* Construct tests so as to minimize the impact of the garbage collector on the
measurement results.
* Write down also the ratio of the worst implementation to the best one. 


## Exercise 3.1
Cache
#### SKILLS:
After this exercise you get the ability to implement cache using Map.
#### GOALS AND OBJECTIVES:
The goal is to test your own implementation of cache.
#### WORK STEPS:
* Create a cache using an available map implementation.
* Test both management strategies.


## Exercise 3.2
Reflections API
#### SKILLS:
After completing this exercise, you will find out how to use reflections for calling any
method of a given object.
#### GOALS AND OBJECTIVES:
The goal is to learn the possibilities of Java reflection API.
#### WORK STEPS:
* Write the interface Testable with the method public void test();.
* Write the class Test implementing the above interface
** Write to the console inside this method
* Write a class calling the test method using reflections.
* Add a private and public field to the Test class and try to access them using
reflections.


## Exercise 4.1
Dynamic proxy
#### SKILLS:
After this exercise you get the ability to implement and use dynamic proxies.
#### GOALS AND OBJECTIVES:
The goal is to implement and test your own dynamic proxy.
#### WORK STEPS:
* Create two classes Test1 and Test2 implementing Testable having the method
public void test().
* The test() implementation should display different messages
* Write a dynamic proxy, which will enable calling test() on a known object
implementing Testable
* Test your code
** Create two dynamic proxy objects
** Call the test() method on both of them